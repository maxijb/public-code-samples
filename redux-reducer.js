import {rpcIds} from '../actions/api-actions';
import {actionTypes} from '../actions/app-actions';
import {handleActions} from 'redux-actions';
import reduceReducers from 'reduce-reducers';
import {createRPCReducer} from '../utils/reducer-utils';

export const initialState = {
  product: {},
  imgReference: {},
  attrReference: {},
  images: [],
  imperfections: [],
  inspections: [],
  categories: [],
  key_options: [],
  allAttributes: [],
  isLoading: false,
  isError: false,
  features: [],
  relatedProducts: {
    isLoading: false,
    list: [],
  },
};

export default reduceReducers(
  (state, action) => state || initialState,

  createRPCReducer(rpcIds['product.relatedProducts'], {
    start: (state, action) => {
      return {
        ...state,
        relatedProducts: {
          isLoading: true,
          list: [],
        },
      };
    },
    success: (state, action) => {
      const resp = action.payload.resp;
      return {
        ...state,
        relatedProducts: {
          isLoading: false,
          list: resp.products,
        },
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        relatedProducts: {
          isLoading: false,
          list: [],
        },
      };
    },
  }),

  createRPCReducer(rpcIds['product.getDetails'], {
    start: (state, action) => {
      return {...state, isLoading: true};
    },
    success: (state, action) => {
      const resp = action.payload.resp;
      return {
        ...state,
        ...resp,
        isLoading: false,
      };
    },
    failure: (state, action) => {
      return {
        ...state,
        ...initialState,
        isLoading: false,
      };
    },
  }),

  handleActions(
    {
      //[actionTypes.openSignupModal]: (state, action) => {
      //  return {...state, openedSignupModal: true, openedSigninModal: false};
      //},
    },
    initialState,
  ),
);
