/* eslint-disable camelcase */
import Seq from 'sequelize';
import {Models} from './index';

// Initialize model
export default function (sequelize) {
  const CategoryImage = sequelize.define(
    'category-image',
    {
      id: {
        type: Seq.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      img_id: {
        type: Seq.INTEGER,
        index: true,
      },
      category_id: {
        type: Seq.INTEGER,
        index: true,
      },
      order: {
        type: Seq.INTEGER,
        index: true,
      },
      attr1: {
        type: Seq.INTEGER,
        index: true,
      },
      attr2: {
        type: Seq.INTEGER,
        index: true,
      },
      attr3: {
        type: Seq.INTEGER,
        index: true,
      },
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['img_id', 'category_id', 'attr1'],
        },
        {
          fields: ['attr3'],
        },
        {
          fields: ['attr2'],
        },
        {
          fields: ['attr1'],
        },
        {
          fields: ['order'],
        },
        {
          fields: ['category_id'],
        },
        {
          fields: ['img_id'],
        },
      ],
    },
  );

  CategoryImage.findAllForCategories = async id => {
    return CategoryImage.findAll({
      where: {
        category_id: id,
      },
      order: [['order', 'ASC']],
      attributes: ['img_id', 'category_id', 'attr1', 'attr2', 'attr3', 'order'],
    });
  };

  return CategoryImage;
}
/* eslint-enable camelcase */
