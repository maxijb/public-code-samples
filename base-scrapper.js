import {Models} from '../../models';
import Logger from '@Common/logger';
import async from 'async';
import Seq from 'sequelize';

class BaseScrapper {

    async processMakes() {

        if (!this.source) {
            throw new Error('No source specified');
        }
       let make = await Models().ScrappedMakeStatus.getNextMake();
       while(make) {
          await Models().ScrappedMakeStatus.saveState({
              make_id: make.id,
              source: this.source,
              state: 'in_progress'
           });
          await this.processMake(make);

          await Models().ScrappedMakeStatus.saveState({
              make_id: make.id,
              source: this.source,
              state: 'done'
           });
          make = await Models().ScrappedMakeStatus.getNextMake();
       }
        Logger.info(`-- Done with all makes for ${this.source}`);
        return;
    }

    async processMake(make) {
        Logger.info('--- Starting process', make);
        const {ScrappedListing, ScrappedCar, ScrappedPrice} = Models();
        let nextData = null
        do {
            const {listings, next} = await this.getSearchPage(make, nextData);

            for (const item of listings) {
                try {
                    let listing = await ScrappedListing.findOne({where: {
                        listing_id: item.listing_id,
                        source: this.source,
                    }
                    });

                    if (!listing) {
                        const car = await ScrappedCar.create({
                            vin: item.vin || null,
                            year: item.year || null,
                            make: item.make || null,
                            model: item.model || null,
                            trim: item.trim || null,
                            type: item.type || null,
                            dealer: item.dealer || null,
                            location: item.location || null,
                            zip: item.zip || null,
                            mileage: item.mileage || null,
                            transmmission: item.transmmission || null,
                            exterior_color: item.exterior_color || null,
                            interior_color: item.interior_color || null,
                            maximum_seating: item.maximum_seating || null,
                            engine: item.engine || null,
                            fuel_type: item.fuel_type || null,
                            stock: item.stock || null,
                            major_options: item.major_options || null,
                            certified: item.certified || null,
                            title_check_issues: item.title_check_issues || null,
                            accidents: item.accidents || null,
                            owners: item.owners || null,
                            additional_information: item.additional_information || null,
                        });


                        listing = await ScrappedListing.create({
                            source: this.source,
                            listing_id: item.listing_id,
                            url: item.url,
                            first_seen: item.first_seen || Seq.fn('CURDATE'),
                            last_seen: Seq.fn('CURDATE'),
                            car_id: car.get('id'),
                        });

                        const details = await this.getDetailsPage({
                            listing_id: item.listing_id,
                            car_id: car.get('id'),
                        });

                        if (details && Object.keys(details).length) {
                            const {car_id, ...other} = details;
                            await ScrappedCar.update(other, {where: {id: car_id}});
                        }
                    } else {
                        listing.last_seen = Seq.fn('CURDATE');
                        await listing.save();
                    }

                    if (item.price) {
                        await ScrappedPrice.create({
                            source: this.source,
                            car_id: listing.get('car_id'),
                            price: item.price,
                            seen: Seq.fn('CURDATE'),
                        }, {
                            ignoreDuplicates: true
                        });
                    }

                } catch(e) {
                    Logger.error(e);
                    continue;
                }
            }

            nextData = next;
        } while(nextData);

        Logger.info(`--- Done with make ${make.name} for ${this.source}`);
    }
}

export default BaseScrapper;
