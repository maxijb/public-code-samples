import {RequestError, RequestErrorMessage} from '@Common/constants/errors';

const Controllers = {
  async getDetails(query, req) {
    const data = await req.Models.Product.getDetails(query.id);

    if (!data) {
      throw new RequestError(RequestErrorMessage.productNotFound);
    }

    return data;
  },

  /* @query {id: category_id}
   */
  async getMinimumPricePerCondition(query, req) {
    return req.Models.Product.getMinimumPricePerCondition(query.id, query.selections);
  },

  /* @query {
       selections: {carrier: id, color: id, appearance: id, capacity: id}
       id: ctageory_id
       lastClicked: last attribute selected
     }
  }
  */
  async loadWithAttributes(query, req) {
    let variant = await req.Models.Product.findVariantForSelections(query);

    if (!variant[0]) {
      return {variant: null, tags: []};
    }

    const tags = await req.Models.AttributeProduct.findAllWithNameForVariant(variant[0].id);

    return {
      variant: variant[0],
      tags,
    };
  },

  /* @query {
    id: product id
  }
  */
  async relatedProducts(query, req) {
    const categories = await req.Models.Category.findRelatedLeaves(query.id);
    const cats = categories.map(m => m.id);
    const products = await req.Models.Product.findCheapestAvailableVersionBatchWithAllColors(cats);

    return {products};
  },
};

export default Controllers;
