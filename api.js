import PublicAPI from './public-api';

const api = {
  health: {controller: 'ok'},
  app: {
    getCarriers: {
      controller: 'app.getCarriers',
    },
    getTimezonesList: {
      controller: 'app.getTimezonesList',
      middleware: ['authorize'],
    },
    getMenuItems: {
      controller: 'app.getMenuItems',
    },
    getHighlightedProducts: {
      controller: 'app.getHighlightedProducts',
    },
  },
  // ---------------------------------- REMOVE THESE 3 ------------------
  getId: {
    controller: 'getId',
    method: 'GET',
    path: 'get-generic-id',
  },
  v2: {
    getId: {
      controller: 'v2GetId',
    },
    getUser: {
      controller: 'v2GetUser',
      method: 'POST',
    },
  },
  // ---------------------------------- UNSUBSCRIBE ------------------
  emailNotifications: {
    unsubscribe: {
      controller: 'user.unsubscribeEmail',
    },
    reenable: {
      controller: 'user.reenableEmail',
    },
  },
  // ---------------------------------- LOGIN AND USER ------------------
  user: {
    signup: {
      controller: 'user.signup',
      method: 'POST',
    },

    login: {
      controller: 'user.login',
      method: 'POST',
    },

    getUserDetails: {
      controller: 'user.getUserDetails',
      middleware: ['authorize'],
    },

    getAssets: {
      controller: 'user.getUserAssets',
      middleware: ['authorize'],
    },

    facebookLogin: {
      controller: 'user.facebookLogin',
      method: 'POST',
    },

    googleLogin: {
      controller: 'user.googleLogin',
      method: 'POST',
    },

    logout: {
      controller: 'user.logout',
      middleware: ['authorize'],
    },

    getUserOwnExtendedDetails: {
      controller: 'user.getUserOwnExtendedDetails',
      middleware: ['authorize'],
    },

    getUserOwnMinimumDetails: {
      controller: 'user.getUserOwnMinimumDetails',
      middleware: ['authorize'],
    },

    sendForgotPassword: {
      controller: 'user.sendForgotPassword',
    },

    validateForgotPasswordToken: {
      controller: 'user.validateForgotPasswordToken',
    },

    validateEmailToken: {
      controller: 'user.validateEmailToken',
    },

    resendValidationToken: {
      controller: 'user.resendValidationToken',
    },

    resetPassword: {
      controller: 'user.resetPassword',
      method: 'POST',
    },

    saveSettings: {
      controller: 'user.saveSettings',
      middleware: ['authorize'],
      method: 'POST',
    },

    createApp: {
      controller: 'user.createApp',
      middleware: ['authorize'],
      method: 'POST',
    },

    isSuperAdmin: {
      controller: 'user.isSuperAdmin',
      middleware: ['authorize'],
    },
  },

  // ---------------------------------- ABS ------------------
  ab: {
    getActiveAB: {
      controller: 'ab.getActiveAB',
    },
  },
  // ---------------------------------- PAYMENTS ------------------
  payments: {
    middleware: ['authorize'],

    performPayment: {
      controller: 'payments.performPayment',
    },
  },

  // ---------------------------------- UPLOAD FILE ------------------
  uploadFile: {
    middleware: ['authorize', 'baseSingleUpload', 'uploadFile'],
    controller: 'ok',
    method: 'POST',
  },
  // ---------------------------------- REPORT ------------------
  report: {
    middleware: ['authorize'],
    interactionsChart: {
      controller: 'report.interactionsChart',
    },
    bookingsChart: {
      controller: 'report.bookingsChart',
    },
    contactsChart: {
      controller: 'report.contactsChart',
    },
    sessionsChart: {
      controller: 'report.sessionsChart',
    },
    sessionsByURLChart: {
      controller: 'report.sessionsByURLChart',
    },
    conversationByOperator: {
      controller: 'report.conversationByOperator',
    },
  },
  // ---------------------------------- PRODUCT ------------------
  product: {
    relatedProducts: {
      controller: 'product.relatedProducts',
    },
    getDetails: {
      controller: 'product.getDetails',
    },
    getMinimumPricePerCondition: {
      controller: 'product.getMinimumPricePerCondition',
    },
    loadWithAttributes: {
      controller: 'product.loadWithAttributes',
    },
  },

  // ---------------------------------- CART ------------------
  cart: {
    addForUser: {
      controller: 'cart.addForUser',
      method: 'POST',
    },
    readActiveForUser: {
      controller: 'cart.readActiveForUser',
    },
    changeQuantity: {
      controller: 'cart.changeQuantity',
      method: 'POST',
    },
    removeItem: {
      controller: 'cart.removeItem',
      method: 'POST',
    },
    getTaxRateByZipCode: {
      controller: 'cart.getTaxRateByZipCode',
    },
    placeOrder: {
      controller: 'cart.placeOrder',
      method: 'POST',
    },
    isOrderMine: {
      controller: 'cart.isOrderMine',
    },
  },
  //attribute: {
  //  controller: 'attribute.loadAll',
  //},
  order: {
    listOrdersForUser: {
      controller: 'order.listOrdersForUser',
    },
  },
  // ---------------------------------- CRUD ------------------
  crud: {
    middleware: ['authorize', 'isSuperAdmin'],
    categoriesList: {
      controller: 'crud.categoriesList',
    },
    categoryGet: {
      controller: 'crud.categoryGet',
    },
    categoryImagesList: {
      controller: 'crud.categoryImagesList',
    },
    categoryImageCreate: {
      controller: 'crud.categoryImageCreate',
    },
    attributesList: {
      controller: 'crud.attributesList',
    },
    categoryCreate: {
      controller: 'crud.categoryCreate',
    },
    categoryUpdate: {
      controller: 'crud.categoryUpdate',
    },
    categoryDelete: {
      controller: 'crud.categoryDelete',
    },
    imagesList: {
      controller: 'crud.imagesList',
    },
    categoryImageUpdate: {
      controller: 'crud.categoryImageUpdate',
      method: 'POST',
    },
    categoryImageDelete: {
      controller: 'crud.categoryImageDelete',
    },
    productsList: {
      controller: 'crud.productsList',
    },
    attributeProductCreate: {
      controller: 'crud.attributeProductCreate',
    },
    attributeProductUpdate: {
      controller: 'crud.attributeProductUpdate',
    },
    attributeProductDelete: {
      controller: 'crud.attributeProductDelete',
    },
    productSave: {
      controller: 'crud.productSave',
      method: 'POST',
    },
    productGet: {
      controller: 'crud.productGet',
    },
  },
  // ---------------------------------- PUBLIC ------------------
  public: {
    middleware: ['cors', 'addPid', 'logSession'],
    ...PublicAPI,
  },
};

export default api;
