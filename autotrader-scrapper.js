import {Models} from '../../models';
import BaseScrapper from './base-scrapper';
import Logger from '@Common/logger';
import request from './proxied-request';
import cheerio from 'cheerio';
import {slugify} from '@Common/utils/urls';

class AutotraderScrapper extends BaseScrapper {
  constructor() {
    super();
    this.source = 'autotrader';
    this.perPage = 50;
  }

  getBonnetData(content) {
    const match = content.match(/__BONNET_DATA__=(.*?)<\/script>/m);
    return JSON.parse(match[1]);
  }

  async getSearchPage(make, next = 0) {
    const url = `https://www.autotrader.com/cars-for-sale/${slugify(
      make.name,
    )}/san-jose-ca-95101?incremental=all&relevanceConfig=default&dma=&searchRadius=0&location=&marketExtension=include&vhrTypes=NO_ACCIDENTS&isNewSearch=true&showAccelerateBanner=false&sortBy=relevance&numRecords=${
      this.perPage
    }&firstRecord=${next * this.perPage}`;

    // https://www.autotrader.com/cars-for-sale/bmw/230i-xdrive/san-francisco-ca-94105?dma=&vhrTypes=NO_ACCIDENTS&searchRadius=0&location=&marketExtension=include&startYear=2000&isNewSearch=false&showAccelerateBanner=false&sortBy=relevance&numRecords=50&firstRecord=50

    // https://www.autotrader.com/cars-for-sale/searchresults.xhtml?zip=94105&listingTypes=used&makeCodeList=CHEV&modelCodeList=CHEV150&searchRadius=0&incremental=all&marketExtension=include&vhrTypes=NO_ACCIDENTS

    let node = null;
    let json = null;
    try {
      const content = await request(url, this);
      const $ = cheerio.load(content);
      node = $('.pagination .glyphicon.glyphicon-menu-right')[0];

      json = this.getBonnetData(content);
    } catch (e) {
      return {listings: [], next: null};
    }

    const listings = Object.keys(json.initialState?.inventory || {})
      .map(id => {
        const data = json.initialState?.inventory?.[id];
        // ignore results that do not match the brand
        if (slugify(data?.make) !== slugify(make.name)) {
          return null;
        }
        const listing = {
          listing_id: id,
          fuel_type: data.fuelType,
          make: data.make,
          model: data.model,
          dealer: data.ownerName,
          price: data.pricingDetail.salePrice,
          major_options: data.quickViewFeatures?.join('\n'),
          transmmission: data.specifications?.transmission?.value,
          engine: data.specifications?.engine?.value,
          exterior_color: data.specifications?.color?.value,
          interior_color: data.specifications?.interiorColor?.value,
          mileage: (data.specifications?.mileage?.value || '').replace(/\D/g, ''),
          stock: data.stockNumber,
          type: data.style?.[0],
          trim: data.trim,
          vin: data.vin,
          year: data.year,
          zip: data.zip,
        };
        const carfaxLink = data.productTiles?.find(p => p.tileType === 'CARFAX_APPENDED')?.link
          ?.href;
        if (carfaxLink) {
          listing.carfaxLink = carfaxLink;
        }
        return listing;
      })
      .filter(l => l !== null);

    return {
      listings,
      next: !!node && listings.length ? next + 1 : null,
    };
  }

  async shouldFireError(content) {
    if (content.indexOf('<H1>Access Denied</H1>') != -1) {
      return 'Access Denied';
    }
    return null;
  }

  async getDetailsPage({listing_id, car_id}) {
    const url = `https://www.autotrader.com/cars-for-sale/vehicledetails.xhtml?listingId=${listing_id}`;
    let json = null;
    try {
      const content = await request(url, this);
      json = this.getBonnetData(content);
    } catch (e) {
      return null;
    }

    const vhr = json.initialState?.inventory?.[listing_id]?.vhrPreview;
    const results = {};

    (vhr || []).forEach(v => {
      switch (v) {
        case 'NO_ACCIDENTS_REPORTED':
        case 'NO_FLOOD_WATER_DAMAGE':
          results.accidents = results.accidents || 0;
          break;
        case 'NO_SALVAGE_TITLE':
          results.title_check_issues = results.title_check_issues || 0;
          break;
        case 'SALVAGE_TITLE':
          results.title_check_issues = 1;
          break;
        case 'FLOOD_WATER_DAMAGE':
        case 'ACCIDENTS_REPORTED':
          results.accidents = 1;
          break;
        case 'ONE_OWNER':
          results.owners = 1;
          break;
        case 'NO_ONE_OWNER':
          results.owners = 3;
          break;
      }
    });

    const ownerKey = Object.keys(json.initialState?.owners || {})?.[0] || null;
    const address = json.initialState?.owners?.[ownerKey]?.location?.address;
    if (address?.city && address?.state) {
      results.location = `${address.city}, ${address.state}`;
    }
    if (Object.keys(results).length) {
      results.car_id = car_id;
    }
    return results;
  }
}

export default AutotraderScrapper;
