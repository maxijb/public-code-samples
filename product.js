import React, {useState, useEffect, useCallback, useMemo} from 'react';
import Link from 'next/link';
import Router from 'next/router';

import {getPageLink} from '@Common/utils/urls';
import Logger from '@Common/logger';
import {Grid} from 'semantic-ui-react';
import Api from '../shared/actions/api-actions';
import {PageID} from '@Common/constants/app';

import {withI18next} from '../shared/i18n/with-i18n.js';
import Header from '../shared/components/header/header';
import Footer from '../shared/components/footer/footer';
import ImageBox from '@Components/product/image-box';
import PurchaseBox from '@Components/product/purchase-box';

import {RequestErrorMessage} from '@Common/constants/errors';
import classnames from 'classnames';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';
import ProductUSPS from '@Components/product/product-usps';
import DetailTabs from '@Components/product/detail-tabs';
import GeneralOverview from '@Components/product/general-overview';
import EquipmentDetail from '@Components/product/equipment-details';
import Inspection from '@Components/product/inspection';

import styles from '@Common/styles/styles.scss';
import css from './product.scss';

function Product(props) {
  const {app, api, user, product, actions, t} = props;

  const [selections, setSelectionsState] = useState({});

  return (
    <>
      <Header {...props} current={PageID.buy} />
      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className={classnames(styles.FlexSpaceBetween, styles.MarginBottom)}>
                <div>
                  <a className={css.backLink}>
                    {'< '} {t('Back to Results')}
                  </a>
                </div>
                <div>
                  <a>{t('Share')}</a>
                </div>
              </div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <ImageBox imgReference={product.imgReference} images={product.images} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <PurchaseBox product={product} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <ProductUSPS />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <DetailTabs product={product} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <GeneralOverview product={product} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <EquipmentDetail product={product} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <Inspection product={product} />
            </Grid.Column>
          </Grid.Row>
        </>
      </Grid>
      <Footer {...props} />
    </>
  );
}

Product.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {
  const [product] = await Promise.all([dispatch(Api.product.getDetails({id: query.id}, req))]);

  if (product?.resp?.error && product?.resp?.message === RequestErrorMessage.productNotFound) {
    if (res) {
      res.redirect('/404');
    } else {
      Router.push('/404');
    }
  }
};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(Product)));
