import React, {useState, useEffect, useCallback, useMemo} from 'react';
import Link from 'next/link';

import {getPageLink} from '@Common/utils/urls';
import Logger from '@Common/logger';
import {Grid, Button} from 'semantic-ui-react';
import Api from '../shared/actions/api-actions';
import {PageID} from '@Common/constants/app';

import Separator from '@Components/display/separator';
import USPIcon from '@Components/display/usp-icon';
import {withI18next} from '../shared/i18n/with-i18n.js';
import Header from '../shared/components/header/header';
import Footer from '../shared/components/footer/footer';
import ImageBox from '@Components/product/image-box';
import PurchaseBox from '@Components/product/purchase-box';

import {RequestErrorMessage} from '@Common/constants/errors';
import classnames from 'classnames';
import WithPublicContext from '@Components/hocs/with-public-context';
import {withAppConsumer} from '../shared/components/app-context';

import Input from '@Components/inputs/wrapped-input';
import Textarea from '@Components/inputs/wrapped-textarea';

import Collage from '@Components/display/collage';

import styles from '@Common/styles/styles.scss';
import css from './sell.scss';

function Product(props) {
  const {app, api, user, product, actions, t} = props;

  const [selections, setSelectionsState] = useState({});

  const setInput = () => {};
  return (
    <>
      <Header {...props} current={PageID.sell} />
      <Grid columns={16} className={classnames(styles.MainGrid)}>
        <>
          <Grid.Row
            stretched
            reversed="mobile"
            verticalAlign="middle"
            className={classnames(styles.initialMargin)}
          >
            <Grid.Column computer={8} mobile={16} tablet={8}>
              <div className={styles.pageTitle}>{t('sellUsYourVehicle')}</div>
              <div className={styles.pageSubtext}>{t('motorennSpecializes')}</div>
              <div className={styles.pageText}>
                {t('ourDeppInsight')
                  .split('\n')
                  .map(p => (
                    <p>{p}</p>
                  ))}
              </div>
            </Grid.Column>
            <Grid.Column computer={8} mobile={16} tablet={8}>
              <Collage />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <Separator />
              <div className={styles.section}>
                <div className={styles.sectionTitle}>{t('whySellYourCarToMotorenn')}</div>

                <div
                  className={classnames(
                    styles.sectionWithBackground,
                    styles.FlexCenter,
                    styles.MarginBottom,
                  )}
                >
                  <div className={classnames(styles.MarginDoubleRight, styles.MarginDoubleLeft)}>
                    <USPIcon size={80} />
                  </div>
                  <div>
                    <div className={styles.MediumBold}>Save Time</div>
                    <div className={classnames(styles.Small, styles.ColorSubtext)}>
                      Your time is valuable and you would rather spend your time
                    </div>
                  </div>
                </div>

                <div
                  className={classnames(
                    styles.sectionWithBackground,
                    styles.FlexCenter,
                    styles.MarginBottom,
                  )}
                >
                  <div className={classnames(styles.MarginDoubleRight, styles.MarginDoubleLeft)}>
                    <USPIcon size={80} />
                  </div>
                  <div>
                    <div className={styles.MediumBold}>Get Cash Today</div>
                    <div className={classnames(styles.Small, styles.ColorSubtext)}>
                      Your time is valuable and you would rather spend your time
                    </div>
                  </div>
                </div>

                <div
                  className={classnames(
                    styles.sectionWithBackground,
                    styles.FlexCenter,
                    styles.MarginBottom,
                  )}
                >
                  <div className={classnames(styles.MarginDoubleRight, styles.MarginDoubleLeft)}>
                    <USPIcon size={80} />
                  </div>
                  <div>
                    <div className={styles.MediumBold}>Eliminate Stress and Uncertainty</div>
                    <div className={classnames(styles.Small, styles.ColorSubtext)}>
                      Your time is valuable and you would rather spend your time
                    </div>
                  </div>
                </div>
              </div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className={classnames(styles.Centered, css.sectionWithWholeBorder)}>
                <div className={classnames(styles.Large, styles.MarginBottom)}>
                  {t('callUsOrVisitToGetOffer')}
                  <b className={styles.ColorAccent}> 4240-363-7410</b>
                </div>
                <div className={classnames(styles.Medium, styles.MarginDoubleBottom)}>
                  {t('stopByTodayLong')}
                </div>
                <div>
                  <Button primary className={styles.MarginDoubleRight}>
                    {t('scheduleAnappointment')}
                  </Button>
                  <Button basic color="blue">
                    {t('remoteAppraisal')}
                  </Button>
                </div>
              </div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className={classnames(styles.Centered, css.sectionWithLightBackround)}>
                <div className={styles.Medium}>{t('motorennWillBeat')}</div>
                <div className={styles.Medium}>
                  {t('XforAXVehicle', {amount: '$3,000', price: '$200,000'})}
                </div>
              </div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className={classnames(styles.Centered, css.sectionWithDarkerBackround)}>
                <div className={styles.FlexSpaceBetween}>
                  <div className={classnames(styles.LargeRegular, styles.ColorWhite, styles.Left)}>
                    {t('motorennPriceMatchCompetitors')}
                  </div>
                  <div className={css.competitors}>
                    <div className={css.row}>
                      <div>CARMAX</div>
                      <div>CARVANA</div>
                    </div>
                    <div className={css.row}>
                      <div>SHIFT</div>
                      <div>VROOM</div>
                    </div>
                  </div>
                </div>
                <div
                  className={classnames(
                    styles.Small,
                    styles.ColorWhite,
                    styles.MarginDoubleTop,
                    styles.Left,
                  )}
                >
                  <b>{t('note')}: </b>
                  {t('motorennPriceMatchCompetitorsFootnote')}
                </div>
              </div>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className={classnames(styles.form, styles.Centered, css.form)}>
                <div className={classnames(styles.Large, styles.MarginDoubleBottom)}>
                  {t('whatCarToTrade')}
                </div>

                <Grid columns={16}>
                  <Grid.Row>
                    <Grid.Column width={8}>
                      <Input placeholder={t('vin')} name="vin" fluid onChange={setInput} />
                    </Grid.Column>
                    <Grid.Column width={8}>
                      <Input placeholder={t('year')} name="year" fluid onChange={setInput} />
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column width={8}>
                      <Input placeholder={t('make')} name="make" fluid onChange={setInput} />
                    </Grid.Column>
                    <Grid.Column width={8}>
                      <Input placeholder={t('model')} name="model" fluid onChange={setInput} />
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column width={8}>
                      <Input placeholder={t('trim')} name="trim" fluid onChange={setInput} />
                    </Grid.Column>
                    <Grid.Column width={8}>
                      <Input
                        placeholder={t('currentMileage')}
                        name="mileage"
                        fluid
                        onChange={setInput}
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Grid.Row>
                  <Grid.Column columns={16} centered className={styles.MarginDoubleTop}>
                    <Button color="black">{t('submit')}</Button>
                  </Grid.Column>
                </Grid.Row>
              </div>
            </Grid.Column>
          </Grid.Row>
        </>
      </Grid>
      <Footer {...props} />
    </>
  );
}

Product.getInitialProps = async ({req, reduxStore, dispatch, query, res}) => {};

export default withAppConsumer(WithPublicContext(withI18next(['home', 'common'])(Product)));
